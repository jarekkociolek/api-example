# Solution structure

## Core - use cases, entities and external interfaces, no network connections, databases etc. -> unit testing
## Infrastructure - connection with database, integration with other external services etc. -> integration testing
## API - api and starting point for our application

https://docs.microsoft.com/pl-pl/dotnet/architecture/modern-web-apps-azure/common-web-application-architectures

