﻿using api_example.Core.DTOs;
using api_example.Core.Entities;
using AutoMapper;

namespace api_example.Core.AutoMapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Order, OrderDTO>().ReverseMap();
        }
    }
}
