﻿using System.Collections.Generic;

namespace api_example.Core.Entities
{
    public class Order : Entity
    {
        public ICollection<Product> Products { get; set; }
        public int InvoiceId { get; set; }
        public Invoice Invoice { get; set; }
        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
