﻿namespace api_example.Core.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}
