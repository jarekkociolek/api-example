﻿using System.Collections.Generic;

namespace api_example.Core.Entities
{
    public class Client : Entity
    {
        public ICollection<Order> Orders { get; set; }
    }
}
