﻿using System;

namespace api_example.Core.Entities
{
    public class Invoice : Entity
    {
        public string Number { get; set; }
        public DateTimeOffset DueDate { get; set; }
        public DateTimeOffset Date { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}
