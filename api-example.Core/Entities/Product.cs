﻿using System.ComponentModel.DataAnnotations.Schema;

namespace api_example.Core.Entities
{
    public class Product : Entity
    {
        public int Name { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Price { get; set; }
    }
}
