﻿using api_example.Core.Entities.External;
using api_example.Core.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace api_example.Core.Services
{
    public class ExternalUserService : IExternalUserService
    {
        private readonly IApiInvoker _apiInvoker;

        public ExternalUserService(IApiInvoker apiInvoker)
        {
            _apiInvoker = apiInvoker;
        }
        public async Task<ICollection<User>> GetAllExternalUsers()
        {
            return await _apiInvoker.GetAsync<ICollection<User>>("https://jsonplaceholder.typicode.com/users"); 
        }
    }
}
