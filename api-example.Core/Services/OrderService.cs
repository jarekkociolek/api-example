﻿using api_example.Core.DTOs;
using api_example.Core.Interfaces;
using AutoMapper;
using System.Threading.Tasks;

namespace api_example.Core.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepository orderRepository, IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<OrderDTO> GetById(int id)
        {
            var order = await _orderRepository.GetById(id);

            return _mapper.Map<OrderDTO>(order);
        }
    }
}
