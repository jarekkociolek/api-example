﻿using api_example.Core.DTOs;
using System.Threading.Tasks;

namespace api_example.Core.Services
{
    public interface IOrderService
    {
        Task<OrderDTO> GetById(int id);
    }
}
