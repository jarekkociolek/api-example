﻿using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using api_example.Core.AutoMapper;
using api_example.Core.Services;
using api_example.Core.Interfaces;

namespace api_example.Core
{
    public static class CoreExtensions
    {
        public static IServiceCollection AddCoreModule(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();

            services.AddSingleton(mapper);
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IExternalUserService, ExternalUserService>();
            return services;
        }
    }
}