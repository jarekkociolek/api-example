﻿using api_example.Core.Entities.External;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace api_example.Core.Interfaces
{
    public interface IExternalUserService
    {
        Task<ICollection<User>> GetAllExternalUsers();
    }
}
