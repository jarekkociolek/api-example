﻿namespace api_example.Core.Interfaces
{
    public interface IJwtGenerator
    {
        string GenerateToken();
    }
}
