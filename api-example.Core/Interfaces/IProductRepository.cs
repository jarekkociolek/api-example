﻿using api_example.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace api_example.Core.Interfaces
{
    public interface IProductRepository
    {
        Task<ICollection<Product>> GetAllProducts();
        
        Task<Product> Update(Product product);

        Task<Product> GetById(int id);

        Task Delete(int id);
    }
}
