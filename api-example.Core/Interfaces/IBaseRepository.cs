﻿using api_example.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace api_example.Core.Interfaces
{
    public interface IBaseRepository<T> where T : Entity
    {
        Task<ICollection<T>> GetAllAsync();

        Task<T> Update(T product);

        Task<T> GetById(int id);

        Task Delete(int id);
    }
}
