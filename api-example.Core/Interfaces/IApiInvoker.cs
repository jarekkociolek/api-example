﻿using System.Threading.Tasks;

namespace api_example.Core.Interfaces
{
    public interface IApiInvoker
    {
        Task<Response> GetAsync<Response>(string url, string token = null);
        Task<Response> PostAsync<Request, Response>(string url, Request request, string token = null);

        Task<Response> PutAsync<Request, Response>(string url, Request request, string token = null);

        Task<Response> DeleteAsync<Request, Response>(string url, string token = null);

        Task<Response> PatchAsync<Request, Response>(string url, Request request, string token = null);
    }
}
