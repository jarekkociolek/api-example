﻿using api_example.Core.Entities;

namespace api_example.Core.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
    }
}
