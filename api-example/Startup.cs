﻿using api_example.Auth;
using api_example.Configuration;
using api_example.Core;
using api_example.Infrastructure.EF;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using System.Text;

namespace api_example
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        //Injected configuration based on appSettings
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.Configure<SqlOptions>(Configuration);

            var sqlOptions = new SqlOptions();
            Configuration.GetSection("Sql").Bind(sqlOptions);

            services.AddInfrastructureModule(sqlOptions.ConnectionString);
            services.AddCoreModule();
            services.AddScoped<IJwtProvider, JwtProvider>();

            //Custom extensions method to add swagger services.
            services.AddSwagger();
            services.AddHttpClient();
            services.AddMemoryCache();

            AddJwt(services);
            services.AddSingleton<IAuthorizationHandler, AdministratorRequirementHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();
            app.UseMiddleware<HeadersMiddleware>();

            //Custom extensions method to configure Swagger.
            app.UseSwaggerDoc();
            app.UseAuthentication();
            app.UseMvc();
        }

        private void AddJwt(IServiceCollection services)
        {
            var jwtOptions = new JwtOptions();
            Configuration.GetSection("Jwt").Bind(jwtOptions);
            services.Configure<JwtOptions>(Configuration.GetSection("Jwt"));

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(cfg =>
            {
                cfg.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = jwtOptions.Issuer,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.SecretKey)),
                    ValidateAudience = false,
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin", policy =>
                {
                    policy.AuthenticationSchemes.Add(JwtBearerDefaults.AuthenticationScheme);
                    policy.Requirements.Add(new AdministratorRequirement());
                });
            });
        }
    }
}
