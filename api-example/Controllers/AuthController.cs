﻿using api_example.Auth;
using Microsoft.AspNetCore.Mvc;

namespace api_example.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IJwtProvider _jwtProvider;

        public AuthController(IJwtProvider jwtProvider)
        {
            _jwtProvider = jwtProvider;
        }

        [HttpGet]
        [Route("GenerateToken")]
        public IActionResult GenerateToken()
        {
            return Ok(_jwtProvider.GenerateToken("User"));
        }

        [HttpGet]
        [Route("GenerateAdminToken")]
        public IActionResult GenerateAdminToken()
        {
            return Ok(_jwtProvider.GenerateToken("Admin"));
        }
    }
}