﻿using System.Threading.Tasks;
using api_example.Configuration;
using api_example.Core.DTOs;
using api_example.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace api_example.Controllers
{
    [Route("api/[controller]")]
    [ApiExceptionFilter]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OrderDTO>> Get(int id)
        {
            return await _orderService.GetById(id);
        }
    }
}
