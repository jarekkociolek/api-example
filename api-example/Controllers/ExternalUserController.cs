﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api_example.Configuration;
using api_example.Core.Entities.External;
using api_example.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace api_example.Controllers
{
    [Route("api/[controller]")]
    [ApiExceptionFilter]
    public class ExternalUserController : Controller
    {
        private readonly IExternalUserService _externalUserService;
        private readonly IMemoryCache _cache;

        private const string ExternalUsersCache = "external-users";

        public ExternalUserController(IExternalUserService externalUserService, IMemoryCache cache)
        {
            _externalUserService = externalUserService;
            _cache = cache;
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<ICollection<User>> Get()
        {
            if(_cache.TryGetValue(ExternalUsersCache, out ICollection<User> users))
            {
                return users;
            }

            var result =  await _externalUserService.GetAllExternalUsers();
            _cache.Set(ExternalUsersCache, result, TimeSpan.FromHours(1));

            return result;
        }
    }
}
