﻿using Microsoft.AspNetCore.Builder;

namespace api_example
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseSwaggerDoc(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api Example v1");
                c.RoutePrefix = string.Empty;
            });

            return app;
        }
    }
}
