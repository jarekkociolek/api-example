﻿namespace api_example.Auth
{
    public interface IJwtProvider
    {
        JsonWebToken GenerateToken(string role);
    }
}
