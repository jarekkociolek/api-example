﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace api_example.Configuration
{
    public class AdministratorRequirementHandler : AuthorizationHandler<AdministratorRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
            AdministratorRequirement requirement)
        {
            if(context.User.IsInRole(requirement.RoleName))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
