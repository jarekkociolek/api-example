﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace api_example.Configuration
{
    public class ApiExceptionFilter : Attribute, IExceptionFilter
    {
        private const string FriendlyErrorMessage = "Error occured but handled, don't worry :)";

        public void OnException(ExceptionContext context)
        {
            context.Result = new ObjectResult(FriendlyErrorMessage);
        }
    }
}
