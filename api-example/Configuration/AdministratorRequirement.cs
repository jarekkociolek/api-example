﻿using Microsoft.AspNetCore.Authorization;

namespace api_example.Configuration
{
    public class AdministratorRequirement : IAuthorizationRequirement
    {
        public string RoleName { get; set; }
    }
}
