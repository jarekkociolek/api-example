﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace api_example
{
    public class HeadersMiddleware
    {
        private readonly RequestDelegate _next;

        public HeadersMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            httpContext.Response.Headers.Add("InternalHeader", "InternalHeaderValue");

            await _next.Invoke(httpContext);
        }
    }
}
