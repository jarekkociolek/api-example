﻿using api_example.Core.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace api_example.Infrastructure.ExternalAPI
{
    public class ApiInvoker : IApiInvoker
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly HttpClient _httpClient;

        public ApiInvoker(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            _httpClient = _httpClientFactory.CreateClient();
        }

        public async Task<Response> GetAsync<Response>(string url, string token = null)
        {
            return await CreateRequest<Response>(url, HttpMethod.Get, token);
        }

        public async Task<Response> PostAsync<Request, Response>(string url, Request request, string token = null)
        {
            return await CreateRequest<Response>(url, HttpMethod.Post, request, token);
        }

        public async Task<Response> PutAsync<Request, Response>(string url, Request request, string token = null)
        {
            return await CreateRequest<Response>(url, HttpMethod.Put, request, token);
        }

        public async Task<Response> DeleteAsync<Request, Response>(string url, string token = null)
        {
            return await CreateRequest<Response>(url, HttpMethod.Delete, token);
        }

        public async Task<Response> PatchAsync<Request, Response>(string url, Request request, string token = null)
        {
            return await CreateRequest<Response>(url, HttpMethod.Patch, request, token);
        }

        private async Task<Response> CreateRequestMessage<Response>(
            string url,
            HttpMethod method,
            string token,
            Func<HttpRequestMessage, Task<Response>> functor)
        {
            using (var msg = new HttpRequestMessage())
            {
                msg.RequestUri = new Uri(url);
                msg.Method = method;
                if (token != null)
                    msg.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);
                return await functor(msg);
            }
        }

        private async Task<Response> CreateRequest<Response>(
            string url,
            HttpMethod method,
            string token)
        {
            return await CreateRequestMessage(url, method, token, async (msg) =>
            {
                return await GetResult<Response>(msg);
            });
        }

        private async Task<Response> CreateRequest<Response>(
            string url,
            HttpMethod method,
            object input,
            string token)
        {
            return await CreateRequestMessage(url, method, token, async (msg) =>
            {
                using (var content = new StringContent(JObject.FromObject(input).ToString()))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    msg.Content = content;
                    return await GetResult<Response>(msg);
                }
            });
        }

        private async Task<Response> GetResult<Response>(HttpRequestMessage msg)
        {
            using (var response = await _httpClient.SendAsync(msg))
            {
                using (var content = response.Content)
                {
                    var responseContent = await content.ReadAsStringAsync();
                    if (!response.IsSuccessStatusCode)
                        throw new Exception(responseContent);
                    if (typeof(IConvertible).IsAssignableFrom(typeof(Response)))
                        return (Response)Convert.ChangeType(responseContent, typeof(Response));
                    return JToken.Parse(responseContent).ToObject<Response>();
                }
            }
        }
    }
}