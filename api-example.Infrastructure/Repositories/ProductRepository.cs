﻿using api_example.Core.Entities;
using api_example.Core.Interfaces;
using api_example.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace api_example.Infrastructure.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly StoreContext _context;

        public ProductRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task<ICollection<Product>> GetAllProducts()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<Product> Update(Product product)
        {
            _context.Products.Update(product);
            await _context.SaveChangesAsync();

            return product;
        }

        public async Task<Product> GetById(int id)
        {
            var product = await _context.Products
                .AsNoTracking()
                .SingleOrDefaultAsync(t => t.Id == id);

            return product;
        }

        public async Task Delete(int id)
        {
            var product = await _context.Products.FindAsync(id);

            _context.Products.Remove(product);
            _context.SaveChanges();
        }
    }
}
