﻿using System.Collections.Generic;
using System.Threading.Tasks;
using api_example.Core.Entities;
using api_example.Core.Interfaces;
using api_example.Infrastructure.EF;
using Microsoft.EntityFrameworkCore;

namespace api_example.Infrastructure.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T> where T : Entity
    {
        private readonly StoreContext _context;

        public BaseRepository(StoreContext context)
        {
            _context = context;
        }

        public async Task Delete(int id)
        {
            var entity = await _context.Set<T>().FindAsync(id);

            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public async Task<ICollection<T>> GetAllAsync()
        {
            return await _context.Set<T>()
                .ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            var entity = await _context.Set<T>()
                .AsNoTracking()
                .SingleOrDefaultAsync(t => t.Id == id);

            return entity;
        }

        public async Task<T> Update(T entity)
        {
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync();

            return entity;
        }
    }
}
