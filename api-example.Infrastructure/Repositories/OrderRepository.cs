﻿using api_example.Core.Entities;
using api_example.Core.Interfaces;
using api_example.Infrastructure.EF;

namespace api_example.Infrastructure.Repositories
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(StoreContext context) : base(context)
        {
        }
    }
}
