﻿using api_example.Core.Interfaces;
using api_example.Infrastructure.ExternalAPI;
using api_example.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace api_example.Infrastructure.EF
{
    public static class DataAccessExtensions
    {
        public static IServiceCollection AddInfrastructureModule(this IServiceCollection services, string connectionString)
        {
            //using of AddDbContextPool because of dbcontext reusability
            //https://docs.microsoft.com/en-us/ef/core/what-is-new/ef-core-2.0#dbcontext-pooling
            services.AddDbContextPool<StoreContext>(options => {
                options.UseSqlServer(connectionString, opt =>
                {
                    opt.MigrationsAssembly("api-example.Migrations");
                });

            });

            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IApiInvoker, ApiInvoker>();

            return services;
        }
    }
}
