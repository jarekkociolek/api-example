﻿using api_example.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace api_example.Infrastructure.EF
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {          

        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>()
                .HasOne(a => a.Invoice)
                .WithOne(a => a.Order)
                .HasForeignKey<Invoice>(c => c.OrderId);
        }
    }
}
